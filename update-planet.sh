#!/bin/sh

TS=$(date --iso=sec -u |sed -r 's,.00:00$,,g' |tr -c -d '[0-9]')
IMAGE=gentoo/planet-pluto
TAG=latest

USER=${1:-gplanet}
GROUP=${2:-gplanet}

mkdir -p htdocs data
chown -R $USER:$GROUP htdocs data

docker run \
	--rm \
	--privileged \
	--restart no \
	--mount type=bind,source="$(pwd)"/htdocs,target=/var/www/planet.gentoo.org/build \
	--mount type=bind,source="$(pwd)"/data,target=/var/www/planet.gentoo.org/data \
	--name planet-pluto_${TS} \
	${IMAGE}:${TAG}
